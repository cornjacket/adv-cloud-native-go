module bitbucket.org/cornjacket/adv-cloud-native-go/Communication/Go-Micro/greeter

go 1.13

require (
	github.com/golang/protobuf v1.4.3
	github.com/micro/go-micro v1.18.0
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
)
