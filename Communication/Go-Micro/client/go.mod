module bitbucket.org/cornjacket/adv-cloud-native-go/Communication/Go-Micro/client

go 1.13

require (
	bitbucket.org/cornjacket/adv-cloud-native-go/Communication/Go-Micro/greeter v0.0.0-20201117072306-9d65a21322f5
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/micro/go-micro v1.18.0
	github.com/micro/go-plugins/wrapper/breaker/hystrix v0.0.0-20200119172437-4fe21aa238fd
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
)
