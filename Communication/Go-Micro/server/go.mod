module bitbucket.org/cornjacket/adv-cloud-native-go/Communication/Go-Micro/server

go 1.13

require (
	bitbucket.org/cornjacket/adv-cloud-native-go/Communication/Go-Micro/greeter v0.0.0-20201117072306-9d65a21322f5
	github.com/micro/go-micro v1.18.0
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
)
